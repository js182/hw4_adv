const request = new XMLHttpRequest();
request.open('GET', 'https://ajax.test-danit.com/api/swapi/films');
request.responseType = 'json';
request.send();

request.onload = () => {
  const films = request.response;
  const filmsList = document.createElement('ul');
  filmsList.className = 'films-list';

  for (let i = 0; i < films.length; i++) {
    const filmItem = document.createElement('li');
    const filmTitle = document.createElement('h2');
    const filmDescription = document.createElement('p');
    const charactersList = document.createElement('ul');
    charactersList.className = 'characters-list';
    filmTitle.textContent = `${films[i].name} (Episode ${films[i].episodeId})`;
    filmDescription.textContent = films[i].openingCrawl;

    for (let j = 0; j < films[i].characters.length; j++) {
      const characterRequest = new XMLHttpRequest();
      characterRequest.open('GET', films[i].characters[j]);
      characterRequest.responseType = 'json';
      characterRequest.send();
      characterRequest.onload = () => {
        const characterData = characterRequest.response;
        const characterItem = document.createElement('p');
        characterItem.textContent = characterData.name;
        charactersList.append(characterItem);
      };
    }

    filmItem.append(filmTitle);
    filmItem.append(filmDescription);
    filmItem.append(charactersList);
    filmsList.append(filmItem);
  }

  document.body.append(filmsList);
};
